/*
  Warnings:

  - A unique constraint covering the columns `[emai]` on the table `users` will be added. If there are existing duplicate values, this will fail.

*/
-- CreateIndex
CREATE UNIQUE INDEX "users_emai_key" ON "users"("emai");
